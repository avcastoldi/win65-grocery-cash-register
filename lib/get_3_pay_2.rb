##
# Item decorator for Get 3 Pay 2 promotions.
# It checks how many items like this are in the bill and gives
# one each three free
#
class Get3Pay2 < SimpleDelegator
  # Updates the item name to show it's a free item
  def name
    if price.zero?
      "#FREE-Get3Pay2-#{__getobj__.name}"
    else
      __getobj__.name
    end
  end

  # The discount will be applied only it item belongs to a bill
  # and it is the third (or a multiple of 3).
  # An item is assigned to a bill using the +BillItem+ delegator.
  def price
    if respond_to?(:bill) &&
       ((bill.items.select { |i| i.id == id }.find_index(self) + 1) % 3).zero?
      0
    else
      __getobj__.price
    end
  end
end
