require_relative '../../lib/item'
##
# Data values used in tests. In a real project I should use
# FactoryGirl of Rails fixtures to do that. I'm keeping it as is
# just to make the project simpler.
#
class Fixtures
  def self.apple
    item = Item.new
    item.name = 'Apples'
    item.weight = 2
    item.unit = 'pound'
    item.value = 2.49
    item.pricing_strategy = 'WeightBased'
    item
  end

  def self.banana
    item = Item.new
    item.id = 2
    item.name = 'Banana'
    item.weight = 1
    item.unit = 'pound'
    item.value = 1.89
    item.pricing_strategy = 'WeightBased'
    item
  end

  def self.cheese
    item = Item.new
    item.id = 3
    item.name = 'Cheese'
    item.weight = 1
    item.unit = 'kg'
    item.value = 8.89
    item.pricing_strategy = 'Item=>Get3Pay2'
    item
  end

  def self.cheerios
    item = Item.new
    item.name = 'Cherios'
    item.weight = 650
    item.unit = 'g'
    item.value = 56
    item
  end

  def self.milk
    item = Item.new
    item.id = 1
    item.name = 'Milk'
    item.weight = 1
    item.unit = 'l'
    item.value = 4.75
    item.pricing_strategy = 'Item=>Get3Pay2'
    item
  end
end
