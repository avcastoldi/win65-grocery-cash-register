require 'minitest/autorun'
require_relative './support/fixtures'
require_relative '../lib/item'
require_relative '../lib/bill'
require_relative '../lib/get_3_pay_2'

class TestGet3Pay2 < Minitest::Test
  def setup
    @item = Fixtures.cheese
    @bill = Bill.new
    4.times { @bill.add(@item) }
  end

  def test_first_and_second_cheese_would_be_charged
    expected_name = 'Cheese'
    expected_price = 8.89

    [0, 1, 3].each do |i|
      assert_equal expected_name,  @bill.items[i].name
      assert_equal expected_price, @bill.items[i].price
    end
  end

  def test_third_cheese_would_be_free
    expected_name = '#FREE-Get3Pay2-Cheese'
    expected_price = 0

    assert_equal expected_name,  @bill.items[2].name
    assert_equal expected_price, @bill.items[2].price
  end
end
