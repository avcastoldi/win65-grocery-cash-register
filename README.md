# The Grocery Store Cash Register #

In this sample you will implement a grocery store cash register. 
Given a number of items you will be required to calculate the total bill. Items are priced a few of different ways:

* A given price for each item, eg. Boxes of Cheerios are $6.99 each
* A given price by weight, eg. Apples are $2.49 per pound
* Items can be go on sale where you might receive bulk discounts, eg. Buy two get one free sales are also available
* Coupons are available to get money off the bill when the total amount is above a threshold, eg.$5 off when you spend $100 or more

The goal of this exercise is to evaluate your coding style and OO design skills. Please send a zip file of all the source code and project files.

Let us know if you have any questions or comments.
