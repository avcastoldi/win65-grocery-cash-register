##
# Represents a payment made using cash.
#
class Cash
  attr_accessor :value

  def initialize(args = {})
    args.each do |k, v|
      instance_variable_set("@#{k}", v) unless v.nil?
    end
  end
end
