require_relative './bill'

##
# This class is a facade for the remaining classes. It represents a stateful
# cash register. It was designed to behave like a real-world cash machine,
# where you open a bill, add/cancel items and pay the bill before moving to
# the next client.
#
# I choose to let it stateful to keep it simple for a didatical perspective.
# A production register would be stateless and rely on a database to store
# the bills/payments states as the transaction goes.
#
class Register
  def initialize
    new_bill
  end

  #
  # Create a new bill. A production class should not allow creating a new bill
  # before paying the current one (if stateful) or keep track of open, non-paid
  # bills before shipping (stateless)
  #
  def new_bill
    @bill = Bill.new
  end

  #
  # Add an item to the bill.
  #
  def add_item(item)
    @bill.add(item)
  end

  #
  # Remove an item from the bill.
  #
  def delete_item(index)
    @bill.delete(index)
  end

  #
  # Add a payment method. Current implemented methods are +Cash+ and +Coupon+.
  #
  def add_payment_method(method)
    @bill.payment.add(method)
  end

  #
  # Cancel a payment method.
  #
  def cancel_payment_method(index)
    @bill.payment.delete_at(index)
  end

  #
  # Return the bill total value.
  #
  def total
    @bill.total
  end

  #
  # Return the amount that still needs to be paid.
  #
  def balance
    @bill.payment.balance
  end

  #
  # Checks if the current bill is paid in whole and closes it. Return
  # +true+ if it was closed, +false+ if not.
  #
  def close_bill
    @bill = nil if balance.zero?
    @bill.nil?
  end
end
