require 'minitest/autorun'
require_relative './support/fixtures'
require_relative '../lib/item'
require_relative '../lib/weight_based'

class TestWeightBased < Minitest::Test
  def setup
    @item = Fixtures.apple
    @item = WeightBased.new(@item)
  end

  def test_unit_based_pricing
    expected_strategy = 'WeightBased'
    expected_price = 4.98

    assert_equal expected_strategy, @item.pricing_strategy
    assert_equal expected_price,    @item.price
  end
end
