require 'minitest/autorun'
require_relative './support/fixtures'
require_relative '../lib/payment'
require_relative '../lib/bill'
require_relative '../lib/item'
require_relative '../lib/cash'
require_relative '../lib/coupon'

class TestPayment < Minitest::Test
  def setup
    @item = Fixtures.cheerios
    @bill = Bill.new
    2.times { @bill.add(@item) }
    @payment = @bill.payment
  end

  def test_balance
    assert_equal 112, @payment.balance

    @payment.add(Coupon.new(bill: @bill))
    assert_equal 107, @payment.balance

    @payment.add(Cash.new(value: 100))
    assert_equal 7, @payment.balance

    @payment.add(Cash.new(value: 7))
    assert_equal 0, @payment.balance
  end
end
