##
# This class represents an arbitrary item.
#
# Being a sales software, I believe this class will be involved
# in many business scenarios. Then, is to follow the most basic
# SOLID principles:
#
# 1. Single responsibility. This class's responsibility is to
# represents an item. Any other business need (variations in the
# whay it is priced should be delegated for support classes
# 1. Open/closed (open for extension, closed for modification).
# Again, if a change is needed to fulfill the business requirement,
# Then we should extend / compose with this class instead of
# changing it.
class Item
  # data item attribute
  attr_accessor :id, :name, :weight, :unit, :value

  # links to the strategies used to set the price (by weight, by unit,
  # etc.). Examples:
  # * +Item+ - if your product is priced by unit (+default+);
  # * +WeightBased+ - if your product is by priced weight;
  # * +Get3Pay2+ - if your product is priced by unit and the client gets
  # one free if buy 3.
  # You can also chain pricing strategies using a _hash_ _rocket_ (<b>=></b>)
  # separator. Example: <b>UnitBased=>Get3Pay2</b>
  attr_accessor :pricing_strategy

  ##
  # Creates a new item.
  def initialize
    @pricing_strategy = 'Item'
  end

  # returns the item price according to item attributes and pricing
  # strategy (default stragety: +Item+)
  def price
    value
  end
end
