require 'minitest/autorun'
require_relative './support/fixtures'
require_relative '../lib/bill'
require_relative '../lib/item'

class TestItem < Minitest::Test
  def setup
    @bill = Bill.new

    @bill.add(Fixtures.banana)
    2.times { @bill.add(Fixtures.milk) }
    3.times { @bill.add(Fixtures.cheese) }
  end

  def test_add
    assert_equal 29.17, @bill.total
    assert_equal 6, @bill.items.size
  end

  def test_delete
    @bill.delete(1)
    assert_equal 24.42, @bill.total
  end
end
