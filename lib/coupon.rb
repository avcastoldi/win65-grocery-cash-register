require_relative './cash'

##
# Represents a discount coupon
class Coupon < Cash
  attr_accessor :bill

  def value
    if bill.total >= 100
      5
    else
      0
    end
  end
end
