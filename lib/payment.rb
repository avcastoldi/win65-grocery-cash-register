##
# Records a payment made for a bill
#
class Payment
  attr_accessor :date, :bill, :methods

  def initialize(args = {})
    args.each do |k, v|
      instance_variable_set("@#{k}", v) unless v.nil?
    end
    @methods = []
  end

  def add(method)
    methods << method if method.class <= Cash
  end

  def delete(index)
    methods.delete_at(index)
  end

  def balance
    bill.total - methods.inject(0) { |s, m| s + m.value }
  end
end
