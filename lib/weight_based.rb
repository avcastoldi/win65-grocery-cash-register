##
# Sets the item price according to its weight and value
#
class WeightBased < SimpleDelegator
  def price
    value * weight
  end
end
