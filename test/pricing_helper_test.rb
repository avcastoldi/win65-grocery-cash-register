require 'minitest/autorun'
require_relative '../lib/pricing_helper'
require_relative '../lib/item'
require_relative '../lib/weight_based'
require_relative '../lib/get_3_pay_2'

class TestItem < Minitest::Test
  def setup
    @item = Item.new
  end

  def test_unit_based_pricing_as_default_strategy
    assert_equal @item, PricingHelper.build(@item)
  end

  def test_single_custom_strategy
    @item.pricing_strategy = 'WeightBased'

    result = PricingHelper.build(@item)

    assert_equal 'WeightBased', result.class.to_s
    assert_equal @item, result.__getobj__
  end

  def test_chained_strategies
    @item.pricing_strategy = 'WeightBased=>Get3Pay2'

    result = PricingHelper.build(@item)

    assert_equal 'Get3Pay2', result.class.to_s
    assert_equal 'WeightBased', result.__getobj__.class.to_s
    assert_equal @item, result.__getobj__.__getobj__
  end
end
