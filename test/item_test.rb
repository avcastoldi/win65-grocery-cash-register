require 'minitest/autorun'
require_relative './support/fixtures'
require_relative '../lib/item'

class TestItem < Minitest::Test
  def setup
    @item = Fixtures.cheerios
  end

  def test_unit_based_pricing
    expected_strategy = 'Item'
    expected_price = 56

    assert_equal expected_strategy, @item.pricing_strategy
    assert_equal expected_price,    @item.price
  end
end
