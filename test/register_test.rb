require 'minitest/autorun'
require_relative './support/fixtures'
require_relative '../lib/register'
require_relative '../lib/cash'

class TestRegister < Minitest::Test
  def setup
    @register = Register.new
  end

  def test_new_bill
    assert_equal 0, @register.total
    assert_equal 0, @register.balance
  end

  def test_add_delete_item
    @register.add_item(Fixtures.apple)
    assert_equal 4.98, @register.total
    assert_equal 4.98, @register.balance

    @register.add_item(Fixtures.banana)
    assert_equal 6.87, @register.total

    @register.delete_item(0)
    assert_equal 1.89, @register.total
  end

  def test_payment
    @register.add_item(Fixtures.apple)
    @register.add_item(Fixtures.banana)

    @register.add_payment_method(Cash.new(value: 2.38))
    assert_equal 4.49, @register.balance

    @register.add_payment_method(Cash.new(value: 4.49))
    assert_equal 0.00, @register.balance
  end

  def test_close
    @register.add_item(Fixtures.apple)
    @register.add_item(Fixtures.banana)

    @register.add_payment_method(Cash.new(value: 2.38))
    assert !@register.close_bill

    @register.add_payment_method(Cash.new(value: 4.49))
    assert @register.close_bill
  end
end
