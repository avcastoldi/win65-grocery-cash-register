require_relative './weight_based'
require_relative './get_3_pay_2'
##
# Factory class to load decorated items
class PricingHelper
  # builds an item and required princing strategy delegators.
  def self.build(item)
    strategies = item.pricing_strategy.split('=>')
    strategies.reduce(item) do |i, strategy|
      if strategy == 'Item'
        i
      else
        Object.const_get(strategy).new(i)
      end
    end
  end
end
