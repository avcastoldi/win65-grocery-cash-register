require_relative './bill_item'
require_relative './pricing_helper'
require_relative './payment'
##
# A Bill represents a list of items bought by a client in a specific order.
#
class Bill
  attr_accessor :date, :items, :payment, :client

  def initialize
    @items = []
    @payment = Payment.new(bill: self)
  end

  # add an item to the bill
  #
  # ===== Attributes
  # * +item+ - item being added to the bill
  #
  # ===== Return
  # The udated bill
  #
  def add(item)
    item = BillItem.new(item)
    item.bill = self
    item = PricingHelper.build(item)
    @items << item
    self
  end

  # delete an item from the bill
  # ===== Attributes
  # * +id+ - index of line to be deleted
  #
  # ===== Return
  # The deleted item
  #
  def delete(line_id)
    @items.delete_at(line_id)
  end

  # return the sum of bill item's price
  def total
    @items.inject(0) { |s, item| s + item.price }
  end
end
