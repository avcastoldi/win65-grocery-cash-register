require 'minitest/autorun'
require_relative './support/fixtures'
require_relative '../lib/payment'
require_relative '../lib/bill'
require_relative '../lib/item'
require_relative '../lib/coupon'

class TestCoupon < Minitest::Test
  def setup
    @item = Fixtures.cheerios
    @bill = Bill.new
    @bill.add(@item)
    @payment = Payment.new
    @payment.bill = @bill
    @coupon = Coupon.new(bill: @bill)
  end

  def test_bill_over_100_should_get_5_discount
    assert_equal 56, @bill.total
    assert_equal 0, @coupon.value

    @bill.add(@item)
    assert_equal 112, @bill.total
    assert_equal 5, @coupon.value
  end
end
